<?php
/**
 * Класс для работы с внешними файлами
 * AUTHOR: Нейман Евгений
 * 
 * При создании экземпляра класса FileExternal нужно указывать url-адрес файла-картинки, который надо скопировать на сервер
 * При отсутсвии файла или невозможности к нему обратится будет выдана ошибка.
 */
class FileExternal
{
    // содержимое файла в виде строки
    private $file_str;
    // массив частей имени файла (путь, имя, расширение, полное имя)
    private $path_parts;
    // допустимые расширения для картинок
    private $extensions_of_picture = array('jpg', 'gif', 'png');
    
    /**
    * Конструктор класса FileExternal.
    * @param string $file_url url-адрес файла-картинки
    * IMPORTANT: При отсутсвии файла или невозможности к нему обратится будет выдана ошибка.
    */    
    function __construct($file_url)
    {
        // читаем содержимое файла с переменную file_str
        $this->file_str = file_get_contents($file_url);

        // проверка наличия файла
        if ($this->file_str === false) {
            throw new Exception("Missing file $file_url");
            return false;
        }

        // разделение имени файла на части
        $this->path_parts = pathinfo($file_url);
    }

    /**
    * Метод копирования файла в директорию $folder_name.
    * @param string $folder_name путь до конечной папки копирования
    * @param boolean $check_extension нужно ли проверять расширение файла
    * @return boolean удачно ли прошло копирование
    * IMPORTANT: при не удачном копировании или не верном расширении файла будет выведена ошибка.
    */
    public function copy($folder_name = 'pic', $check_extension = true)
    {
        if ($check_extension && !$this->checkExtension()) {
            throw new Exception("Wrong type of file $file_url");
            return false;
        }

        if (!$this->checkFolder($folder_name)) {
            throw new Exception("Missing folder $folder_name");
            return false;
        }

        // зададим конечное имя файла
        $folder_name_with_separator = $this->addDirectorySeparator($folder_name);
        $file_name = $this->path_parts['basename'];
        $final_file_name = $folder_name_with_separator . $file_name;

        if (!file_put_contents($final_file_name, $this->file_str)) {
            throw new Exception("The error has occurred on server file $final_file_name");
            return false;
        }

        return true;
    }

    // метод проверки корректности типа файла
    private function checkExtension()
    {
        if (empty($this->path_parts['extension']) || 
            !in_array($this->path_parts['extension'], $this->extensions_of_picture) ) {
            return false;
        }
        return true;
    }

    // метод проверки наличия конечной папки.
    // при необходимости создадёт её.
    private function checkFolder($folder_name)
    {
        if (!empty($folder_name)) {
            if (!file_exists($folder_name)) {
                return mkdir($folder_name);
            }
        }
        return true;
    }

    // метод, который дописывает разделитель к имени папки
    private function addDirectorySeparator($folder_name)
    {
        if (!empty($folder_name)) {
            return  $folder_name . DIRECTORY_SEPARATOR;
        } else {
            return '';
        }
    }
}
